from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Post
from .forms import CommentForm

def index(request):
    context = {}
    return render(request, 'blog/index.html', context)

def about(request):
    context = {}
    return render(request, 'blog/about.html', context)

def list(request):
	posts = Post.objects.all()

	return render(request, 'blog/list.html', {'posts': posts})

def post_detail(request, slug):
    post = Post.objects.get(slug=slug)

    if request.method == 'POST':
        form = CommentForm(request.POST)
         
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.save()
            
            return redirect('post_detail', slug=post.slug)
    else:
    	form = CommentForm()

    return render(request, 'blog/detail.html', {'post': post, 'form': form})

def CategoryView(request,cats):
    return render(request, 'blog/categories.html', {'cats': cats})