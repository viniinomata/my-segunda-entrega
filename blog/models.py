from django.db import models

class Post(models.Model):
    title = models.CharField(max_length=255)
    category = models.CharField(max_length=255, default="None")
    category2 = models.CharField(max_length=255, default= "None")
    category3 = models.CharField(max_length=255, default= "None")
    slug = models.SlugField()
    intro = models.TextField()
    body = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)

#    class Meta:
#    	ordering = ['-date_added'] # Torna os post cronológicos

class Comment(models.Model):
    post = models.ForeignKey(Post, related_name='comments', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    email = models.EmailField()
    body = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        ordering = ['date_added']
        
class Category(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255, default= "Sem descrição")