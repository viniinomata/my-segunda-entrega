from django import forms
from django.forms.widgets import Select, Textarea

from .models import Comment,Post

class CommentForm(forms.ModelForm):
	class Meta:
		model = Comment
		fields = ['name', 'email', 'body']