from django.urls import path

from . import views
#from blog.views import post_detail

urlpatterns = [
    path('about/', views.about, name='about'),
    path('', views.index, name='index'),
    path('detail/', views.post_detail, name='detail'),
    path('list/', views.list, name='list'),

]